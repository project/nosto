<?php

/**
 * @file
 */

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @see hook_form_FORM_ID_alter()
 */
function nosto_commerce_form_nosto_admin_form_alter(&$form, &$form_state, $form_id) {
  $form['commerce'] = [
    '#type' => 'fieldset',
    '#title' => t('Commerce'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'vertical_tabs',
    '#access' => user_access('administer nosto commerce')
  ];

  $options = _nosto_commerce_get_master_product_options();
  switch (count($options)) {
    case 0:
      $form['commerce']['nosto:master_product'] = [
        '#type' => 'value',
        '#value' => NULL,
        '#required' => TRUE
      ];
      $form['commerce']['nosto:error'] = [
        '#type' => 'markup',
        '#markup' => t('Currently this module only supports master product installations'),
      ];
      break;
    case 1:
      $form['commerce']['nosto:master_product'] = [
        '#type' => 'item',
        '#title' => t('Master products content type and field') . ':',
        '#value' => key($options),
        '#markup' => reset($options)
      ];
      break;
    default:
      $form['commerce']['nosto:master_product'] = [
        '#type' => 'select',
        '#title' => t('Master products content type and field'),
        '#options' => $options
      ];
  }
}

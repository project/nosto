<?php

/**
 * @file
 */

use Nosto\Object\Product\Product;

/**
 * @defgroup nosto_commerce_hooks Nosto Hooks
 *
 * @{
 * Hooks for modules to implement, extend or modify Nosto Commerce.
 *
 * @see https://api.drupal.org/api/drupal/includes%21module.inc/group/hooks/7.x
 */

/**
 * @param \Nosto\Object\Product\Product $payload
 *   The product payload to alter before submission to Nosto.
 * @param \stdClass $master_product
 *   The Drupal master product node object.
 * @param \stdClass[] $commerce_products
 *   The array of commerce product variation objects (for the above master
 *   product).
 * @param array $info
 *   The data that was sent when the product was added to the Nosto queue.
 *
 * @see _nosto_commerce_prepare_payload()
 */
function hook_nosto_commerce_prepare_payload_alter(Product &$payload, \stdClass $master_product, array $commerce_products, array $info) {
  $payload->setDescription($master_product['field_my_description'][LANGUAGE_NONE][0]['safe_value']);
  /** @var \Nosto\Object\Product\Sku $sku */
  foreach ($payload->getSkus() as $sku) {
    $commerce_product = $commerce_products[$sku->getId()];
    $sku->addCustomField('collection', $commerce_product['field_my_collection'][LANGUAGE_NONE][0]['value']);
  }
}

/**
 * @} End of "defgroup nosto_commerce_hooks".
 */

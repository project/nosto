<?php

/**
 * @file
 */

/**
 * Implements hook_permission().
 *
 * @see hook_permission()
 */
function nosto_commerce_permission() {
  $permissions['administer nosto commerce'] = [
    'title' => t('Administer Nosto commerce'),
    'description' => t('Gives access to the nosto commerce tab in the nosto configuration page.'),
    'restrict access' => TRUE
  ];
  return $permissions;
}

<?php

/**
 * @file
 */

/**
 * Implements hook_permission().
 *
 * @see hook_permission()
 */
function nosto_permission() {
  $permissions['run nosto cron'] = [
    'title' => t('Run Nosto cron'),
    'description' => t('Accounts with this permission will be able to run the Nosto cron without the cron key'),
    'restrict access' => TRUE
  ];
  $permissions['administer nosto cron'] = [
    'title' => t('Administer Nosto cron'),
    'description' => t('Gives access to the nosto cron tab in the nosto configuration page.'),
    'restrict access' => TRUE
  ];
  return $permissions;
}

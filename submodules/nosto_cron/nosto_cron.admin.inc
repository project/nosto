<?php

/**
 * @file
 */

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @see hook_form_FORM_ID_alter()
 */
function nosto_cron_form_nosto_admin_form_alter(&$form, &$form_state, $form_id) {
  $form['cron'] = [
    '#type' => 'fieldset',
    '#title' => t('Cron'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'vertical_tabs',
    '#access' => user_access('administer nosto cron')
  ];

  $form['cron']['nosto:cron_key'] = [
    '#type' => 'item',
    '#title' => t('Nosto cron key:'),
    '#value' => variable_get('nosto:cron_key'),
    '#markup' => l(variable_get('nosto:cron_key'), 'cron/nosto/' . variable_get('nosto:cron_key'), [
      'absolute' => TRUE,
      'target' => '_blank'
    ]),
    '#prefix' => '<div id="cron-key-wrapper">',
    '#suffix' => '</div>',
  ];

  $form['cron']['nosto:reset_cron_key'] = [
    '#type' => 'submit',
    '#value' => t('Reset cron key'),
    '#ajax' => [
      'wrapper' => 'cron-key-wrapper',
      'callback' => '_nosto_cron_reset_key_callback',
    ],
    '#submit' => ['_nosto_cron_reset_key_submit'],
    '#limit_validation_errors' => [],
  ];

  $form['cron']['nosto:run_cron'] = [
    '#type' => 'link',
    '#title' => t('Run cron'),
    '#href' => 'admin/config/services/nosto/run-cron',
    '#options' => [
      'absolute' => TRUE,
      'query' => [
        'destination' => current_path()
      ]
    ],
    '#attributes' => [
      'class' => ['button']
    ]
  ];
}

/**
 * Randomizes the Nosto cron key and rebuilds the form.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form state array.
 */
function _nosto_cron_reset_key_submit(array $form, array &$form_state) {
  variable_set('nosto:cron_key', drupal_random_key());
  $form_state['rebuild'] = TRUE;
}

/**
 * Returns the randomized Nosto cron key.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form state array.
 *
 * @return array
 *   The Nosto cron key render array.
 */
function _nosto_cron_reset_key_callback(array $form, array $form_state) {
  return $form['cron']['nosto:cron_key'];
}

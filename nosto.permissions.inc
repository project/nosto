<?php

/**
 * @file
 */

/**
 * Implements hook_permission().
 *
 * @see hook_permission()
 */
function nosto_permission() {
  $permissions['access nosto configuration form'] = [
    'title' => t('Access Nosto configuration form'),
    'description' => t('Accounts with this permission will be allowed to access the nosto configuration page.'),
    'restrict access' => TRUE
  ];
  $permissions['administer nosto account'] = [
    'title' => t('Administer Nosto account'),
    'description' => t('Gives access to the nosto account tab in the nosto configuration page.'),
    'restrict access' => TRUE
  ];
  return $permissions;
}

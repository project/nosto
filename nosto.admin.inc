<?php

/**
 * @file
 * Admin page callbacks for the Nosto module.
 */

use Nosto\NostoException;
use Nosto\Object\Signup\Account;
use Nosto\Request\Api\Token;

/**
 * @defgroup nosto_forms Nosto forms
 * @{
 * Nosto administration forms.
 */

/**
 * @param array $form
 * @return array
 */
function nosto_admin_form(array $form) {
  $form['vertical_tabs'] = array('#type' => 'vertical_tabs');

  _nosto_admin_form_account($form);
  _nosto_admin_form_js($form);

  return system_settings_form($form);
}

/**
 * @} End of "defgroup nosto_forms".
 */

/**
 * @defgroup nosto_forms_callback Nosto forms callbacks and validation.
 * @{
 * Functions that handle user input from forms.
 */

/**
 * @param array $form
 * @param array $form_state
 */
function _nosto_admin_form_validate_account(array $form, array &$form_state) {
  $account_id = $form_state['values']['nosto:account_id'];
  $account_secret = $form_state['values']['nosto:account_secret'];
  try {
    $account = new Account($account_id);
    $token = new Token(Token::API_PRODUCTS, $account_secret);
    $account->addApiToken($token);
  }
  catch (NostoException $e) {
    $element_name = isset($account) ? 'nosto:account_secret' : 'nosto:account_id';
    form_set_error($element_name, $e->getMessage());
  }
}

/**
 * @} End of "defgroup nosto_forms_callback".
 */

/**
 * @param array $form
 */
function _nosto_admin_form_account(array &$form) {
  $form['#validate'][] = '_nosto_admin_form_validate_account';
  $form['account'] = [
    '#type' => 'fieldset',
    '#title' => t('Account'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'vertical_tabs',
    '#access' => user_access('administer nosto account')
  ];

  $account_id = variable_get('nosto:account_id', NULL);
  $form['account']['nosto:account_id'] = [
    '#type' => 'textfield',
    '#title' => t('Account ID'),
    '#default_value' => $account_id,
    '#description' => t('Your Nosto marketing account id.'),
    '#required' => TRUE,
  ];

  $form['account']['nosto:account_secret'] = [
    '#type' => 'textfield',
    '#title' => t('Account secret'),
    '#default_value' => variable_get('nosto:account_secret', NULL),
    '#description' => t('Your Nosto marketing account secret.<br>See %url', [
      '%url' => url(format_string('https://my.nosto.com/admin/@account_id/account/settings/tokens', [
        '@account_id' => $account_id ?: '[ACCOUNT_ID]'
      ]))
    ]),
    '#required' => TRUE,
  ];
}

/**
 * @param array $form
 */
function _nosto_admin_form_js(array &$form) {
  $form['js'] = [
    '#type' => 'fieldset',
    '#title' => t('JavaScript API'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'vertical_tabs',
  ];

  $form['js']['nosto:attach_js'] = [
    '#type' => 'checkbox',
    '#title' => t('Attach nostojs library'),
    '#default_value' => variable_get('nosto:attach_js', FALSE),
    '#description' => t('Mark this checkbox if you want to use the <a href="!url" target="_blank">Nosto JS SDK</a>.', [
      '!url' => url('https://help.nosto.com/get-started/guides/nosto-javascript-api-introduction')
    ])
  ];
}

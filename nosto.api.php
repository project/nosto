<?php

/**
 * @file
 */

/**
 * @defgroup nosto_hooks Nosto Hooks
 *
 * @{
 * Hooks for modules to implement, extend or modify Nosto.
 *
 * @see https://api.drupal.org/api/drupal/includes%21module.inc/group/hooks/7.x
 */

/**
 * Adds a queue worker to the nosto queue workers.
 *
 * @return array
 *   An associative array where the key is the queue name and the value is
 *   again an associative array. Possible keys are:
 *   - 'worker callback': The name of an implementation of
 *     callback_queue_worker().
 *   - 'time': (optional) How much time Drupal should spend on calling this
 *     worker in seconds. Defaults to 15.
 *   - 'skip on cron': (optional) Set to TRUE to avoid being processed during
 *     cron runs (for example, if you want to control all queue execution
 *     manually).
 *
 * @see hook_cron_queue_info()
 */
function hook_nosto_queue_info() {
  $queues['product_upsert']['worker callback'] = 'mymodule_product_upsert';
  return $queues;
}

/**
 * Alters the definitions of Nosto queue workers before nosto cron runs.
 *
 * Called by drupal_cron_run() to allow modules to alter cron queue settings
 * before any jobs are processesed.
 *
 * @param array $queues
 *   An array of Nosto queue information.
 *
 * @see hook_nosto_queue_info()
 * @see hook_cron_queue_info_alter()
 */
function hook_nosto_queue_info_alter(array &$queues) {
  $queues['product_upsert']['worker callback'] = 'other_module_product_upsert';
}

/**
 * @} End of "defgroup nosto_hooks".
 */
